var uuid4 = "^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"

exports.queryMesage = {
  "id": "/queryMesage",
  "type": "object",
  "required": true,
  "properties": {
    "uuid": {
      "type": "string",
      "pattern": uuid4,
      "required": true,
    },
    "sum": {
      "type": "integer",
       "minimum": 0,
       "required": true,
    },
    "user_to": {
      "type": "string",
      "minLength": 6,
      "maxLength": 32,
      "required": true,
    },
    "comment": {
      "type": "string",
      "optional": true,
      "maxLength": 100,
      "required": true,
    }
  }
};

exports.botConfig = {
  "id": "/botConfig",
  "type": "array",
  "required": true,
  "items": {
    "type": "object",
    "required": true,
    "properties":
      {
        "uuid": {
          "type": "string",
          "pattern": uuid4,
          "required": true,
        },
        "w_login": {
          "type": "email",
          "required": true,
        },
        "w_password": {
          "type": "string",
          "minLength": 6,
          "maxLength": 32,
          "required": true,
        },
        "y_login": {
          "type": "email",
          "required": true,
        },
        "y_password": {
          "type": "string",
          "minLength": 6,
          "maxLength": 32,
          "required": true,
        },
        "y_gauth": {
          "type": "string",
          "pattern": "^[A-Z1-9]{32}$",
          "required": true,
        },
        "proxy": {
          "type": "string",
          "format": "uri",
          "optional": true,
        },
        "useragent": {
          "type": "string",
          "optional": true,
        }
    }
  }
};

exports.serviceConfig = {
  "id": "/serviceConfig",
  "type": "object",
  "required": true,
  "properties": {

    "query": {
      "type": "object",
      "properties": {
        "uri": {
          "type": "string",
          "format": "uri",
          "required": true,
        },
        "exchange": {
          "type": "string",
          "required": true,
        },
      }
    },

    "db": {
      "type": "object",
      "required": true,
      "properties": {
        "uri": {
          "type": "string",
          "format": "uri",
          "required": true,
        },
      },
      "params": {
        "type": "object",
        "optional": true,
      }
    },

    "web_bot": {
      "type": "object",
      "required": true,
      "properties": {
        "course": {
          "type": "integer",
          "minimum": 1,
          "required": true,
        },
        "retries": {
          "type": "integer",
          "minimum": 1,
          "optional": true,
        },
        "retries_pause": {
          "type": "integer",
          "minimum": 0,
          "optional": true,
        },
        "fin_retries": {
          "type": "integer",
          "minimum": 1,
          "optional": true,
        },
        "params": {
          "type": "object",
          "optional": true,
        },
        "userData_path": {
          "type": "string",
          "optional": true,
        }
      }
    },

  }
}
