const mockery = require('mockery-next')
const commandLineArgs = require('command-line-args')
const uuidV4 = require('uuid/v4');

var Decimal = require('decimal.js');

var promise_mock = async (name, rvalue, timeout) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('resolved', name, 'after', timeout)
      if (rvalue instanceof Function) {
        resolve(rvalue());
      } else {
        resolve(rvalue);
      }
    }, timeout);
  });
};



var botfull_mock = {
  proces:                    async (bot) => {await promise_mock('proces'        ,  null,  500)}
};
var mock_botfull = () => {mockery.registerMock('./web_bot/bot', botfull_mock);}



var y_data = () => {
  return {
    bot_balance: Decimal(220.51),
    hash:        Math.random().toString(36).substr(2, 16) + Math.random().toString(36).substr(2, 16),
    pname:       "stkirik@mail.ru",
    sum:         Decimal(22),
    com:         Decimal(0),
    sum_all:     Decimal(22),
  }
}

var mock_botpart_w = {
  proces:                async ()    => {await promise_mock('proces_wot'        ,  null,  500)},
};
var mock_botpart_y = {
  auth:               async ()    => {await promise_mock('yandex_auth'          ,  null,  500)},
  get_data:           async ()    => {return await promise_mock('yandex_get_data',  y_data,  500)},
  proces_toclick:     async ()    => {await promise_mock('proces_yandex_toclick'             ,  null,  500)},
  proces_click:       async ()    => {await promise_mock('proces_yandex_click'  ,  null,  500)},
  proces_postclick:   async ()    => {await promise_mock('yandex_post_data'     ,  null,  500)},
};
var mock_botpart = () => {
  mockery.registerMock('./web_bot/yandex', mock_botpart_y);
  mockery.registerMock('./web_bot/wot'   , mock_botpart_w);
  mockery.registerMock('./web_bot/instance', {get_nigtmare: () => {return {end: () => {}}}});
}




  var q_data = () => {
    return {
      message: {
        uuid        : uuidV4(),
        sum         : 100         ,
        user_to     : 'novoxudonoseroO' ,
        comment     : 'asd'     ,
      },
      msg: null
    }
  }

var q = class Query {
  constructor() {};
  async init(){};
  async ack(){};
  async get_message(){
    return await promise_mock('get_message',  q_data,  100)
  };
}

var query_mock = {
  Query       : q,
};
var mock_query   = () => {mockery.registerMock('./query'      , query_mock  );}





var db_mock = {
  createTrans : async (data)   => {
    var transaction = {
      wot_user_to : data.wot_user_to,
      comment     : data.comment,
      sum         : data.sum,
    }
    return await promise_mock('createTrans',  transaction,  100)
  },
  setError    : async (t,e) => {await promise_mock('setError'   ,     e,  100)},
  setInProc   : async ()    => {await promise_mock('setInProc'  ,  null,  100)},
  setYRedy    : async ()    => {await promise_mock('setYRedy'   ,  null,  100)},
  setYdata    : async ()    => {await promise_mock('setYdata'   ,  null,  100)},
  setYDone    : async ()    => {await promise_mock('setYDone'   ,  null,  100)},
  setFinished : async ()    => {await promise_mock('setFinished',  null,  100)},
};
var mock_db     = () => {mockery.registerMock('./db/higlevel' , db_mock  );}

var yandex = require('./web_bot/yandex.js')
var pay_mock = {
  auth : yandex.auth,
  get_data : yandex.get_data,
  proces_toclick : yandex.proces_toclick,
  proces_click : async ()    => {await promise_mock('proces_click',  null,  100)},
  proces_postclick : async ()    => {await promise_mock('proces_postclick',  null,  100)},
}

var mock_pay    = () => {
  mockery.registerMock('./web_bot/yandex' , pay_mock  );
}


var mocked = (query_m, bot_m, db_m, p_m) => {
  if (query_m) (mock_query());
  if (bot_m && db_m) {
    mock_botfull();
  } else {
    if (bot_m) mock_botpart();
  }
  if (db_m) mock_db();
  if (p_m) mock_pay();

  mockery.enable({warnOnUnregistered: false});
  return require('./index.js');
}

const optionDefinitions = [
  { name: 'mock_query', alias: 'q',  type: Boolean },
  { name: 'mock_bot'  , alias: 'b',  type: Boolean },
  { name: 'mock_pay'  , alias: 'p',  type: Boolean },
  { name: 'mock_db'   , alias: 'd',  type: Boolean }
]


if (require.main === module) {
  const options = commandLineArgs(optionDefinitions)
  var index_mocked = mocked(options.mock_query, options.mock_bot, options.mock_db, options.mock_pay);

  (async () => {
    try {
      var inited_data = await index_mocked.init();
      await index_mocked.main_loop(inited_data);
    } catch (e) {
      console.log(e)
    }
  })();

}
