# WOT paybot #

Automate payments.

### How do I get set up? ###

1. Create config files `cp config.example.json config.json && cp bot_config.example.json bot_config.json`.
2. Edit database uri and query uri in `config.json`. Edit `userData_path` to session storage path.
3. Edit `bot_config.json` with your auth data.
4. Install requirements with `npm install`
5. Create database with `node --harmony db/create_tables.js`. (Be careful! It can destroy data in existing database!)

6. Run with `node --harmony --use_strict index.js`.
7. Stop with `kill -s SIGPIPE $PID_OF_YOUR_PROCES`


### Deployment instructions ###

* Commits to master will be deployed via pipelines
* Include [skip ci] to commit message to avoid this

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
