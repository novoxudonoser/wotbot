mkdir -p ~/.ssh
(umask  077 ; echo $DEV_SSH_KEY | base64 --decode > ~/.ssh/id_rsa)
(umask  077 ; echo $DEV_KNOWN_HOSTS | base64 --decode > ~/.ssh/known_hosts)
(umask  077 ; echo $DEV_SSH_CONFIG | base64 --decode > ~/.ssh/config)
sed "s/{branch}/$BITBUCKET_BRANCH/;s/{commit}/$BITBUCKET_COMMIT/" server_build.sh | ssh dev
