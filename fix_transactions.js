var fs = require("fs");
var shema = require('./json_schemas');
var Validator = require('jsonschema').Validator;
var validator = new Validator();

exports.fix = async (condition, next_state, next_progres, db) => {
  var transactions = await db.models.Transaction.findAll({ where: condition })
  for (var db_trans of transactions) {
    var trans = {
      id         : db_trans.id,
      state_id   : db_trans.state_id,
      progres_id : db_trans.progres_id,
      bot_id     : db_trans.bot_id,
    }
    await db.sequelize.transaction({
      isolationLevel: db.Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
        },(t) => {
      return db.higlevel.move_to_next_status(
        trans,
        next_state,
        next_progres
      );
    });
  }
}

exports.fixAll = async (db) => {
  await exports.fix(
    {
      state_id  : db.models.transStateMap.in_progres,
      progres_id: db.models.transProgresMap.wot_started
    },
   db.models.transStateMap.redy,
   db.models.transProgresMap.none,
   db
  )
  await exports.fix(
    {
      state_id  : db.models.transStateMap.in_progres,
      progres_id: {$or: [
       db.models.transProgresMap.y_click_redy,
       db.models.transProgresMap.y_click_done,
      ]}
    },
   db.models.transStateMap.fin_unknown,
   db.models.transProgresMap.none,
   db
  )
}

if (require.main === module) {
  (async () => {
    var config = JSON.parse(fs.readFileSync("config.json"));
    validator.validate(config, shema.serviceConfig, {throwError: true})
    var db = await db_instance.init_db(config.db)

    try {
      await exports.fixAll();
    } catch (e) {
      console.log(e);
      throw e;
    }
  })()
}
