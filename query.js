var rmq = require('amqplib');
var isUtf8 = require('is-utf8');

var shema = require('./json_schemas');
var jsonschema = require('jsonschema')
var Validator = jsonschema.Validator;
var validator = new Validator();

var promiseTimeout = async (timeout) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => { resolve(); }, timeout);
  });
}


exports.Query = class Query {

  constructor(config) {
    this.config = config;
    this.ch = null;
    this.stop = false;
  }
   async init() {
    var conn = await rmq.connect(this.config.uri)

    this.ch = await conn.createChannel();
    var query = await this.ch.assertQueue(this.config.exchange);
    this.ch.prefetch(1); // забираем по 1 сообщению за раз
  }

  async close() {
    return await this.ch.close()
  }

  async ack(msg) {
    await this.ch.ack(msg);
  }

  async get_message() {
    var msg = false
    while(!msg) {
      msg = await this.ch.get(this.config.exchange)
      if(!msg) await promiseTimeout(500);
      if (this.stop) return {message: null, msg: null}
    }

    if (!isUtf8(msg.content)) {
      console.log("QUERY - not utf8", msg.content);
      await this.ch.ack(msg);
      return {message: null, msg: null}
    }
    var text = msg.content.toString();

    try {
      var parsed = JSON.parse(text)
    } catch (e) {
      if (e instanceof SyntaxError) {
        console.log("QUERY - cant parse message", text, e);
        await this.ch.ack(msg);
        return {message: null, msg: null}
      } else {
        throw e
      }
    }

    try {
      validator.validate(parsed, shema.queryMesage, {throwError: true})
    } catch (e) {
      if (e instanceof jsonschema.ValidationError) {
        console.log("QUERY - ValidationError", parsed, e);
        await this.ch.ack(msg);
        return {message: null, msg: null}
      } else {
        throw e
      }
    }

    return {message: parsed, msg}
  }
}
