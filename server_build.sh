domain='webroot'
cd /tmp
cp -HR ./$domain ./deploy
cd ./deploy
git fetch
git checkout {branch}
git pull
sha={commit}
#npm install
cd -
mv ./deploy ./$sha
ln -s $(pwd)/$sha $(pwd)/temp
mv -T ./temp ./webroot
exit
