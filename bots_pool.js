var web_bot = require('./web_bot/bot');
var web_bot_errors = require('./web_bot/errors');

var db_higlevel = require('./db/higlevel');

var promiseTimeout = async (timeout) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => { resolve(); }, timeout);
  });
}

class Bot {

  constructor(data) {
    this.id = data.config.uuid
    this.proces = this // нужно для прохождения первого цикла main_loop
    this.in_work = false
    this.error   = false
    this.nightmare = null
    this.transaction = null
    this.config = data.config
    this.nconfig = data.nconfig
    this.db = data.db
    this.dbBot = data.dbBot
    this.retries = null
    this.fin_retries = 1
  }

  proces_transaction(transaction) {
    var on_fin = (res) =>{
        this.in_work = false;
    }

    this.transaction = transaction

    var data = {
      to:      this.transaction.wot_user_to,
      comment: this.transaction.comment,
      sum:     Number(this.transaction.sum),
      course:  this.nconfig.course
    }

    if (this.in_work) throw new Error("bot " + this.id + " alredy working");
    this.in_work = true
    this.retries = this.retries || 1

    this.proces = web_bot.proces(this, data)
      .then(result => {
        this.error = false;
        this.retries = null;
        on_fin()
        return this;
      })
      .catch(error => {
        error.bot = this
        on_fin()
        throw error;
      })
  }

};

exports.BotPool = class BotPool {

  constructor(bots_config, config, db) {
    this.bots = new Map()
    this.stop = false
    this.bots_config = bots_config
    this.db = db
    this.config = config
  }

  async init() {
    for (var bot_config of this.bots_config) {
      var dbBot = await this.db.higlevel.getBot(bot_config)
      let new_bot = new Bot({
        config: bot_config,
        dbBot: dbBot,
        nconfig: this.config,
        db: this.db,
      });
      this.bots.set(bot_config.uuid, new_bot)
    }
  }

  async proces(transaction, redy_bot) {
    redy_bot.proces_transaction(transaction)
    await this.wait_any_bot_redy();
  }

  async get_redy_bot() {
    var active_bots = new Map();
    for (var bot of this.bots.values()) {
      if (!bot.error && !bot.in_work) {
        bot.dbBot = await this.db.models.Bot.findById(bot.dbBot.id) // обновление баланса
        var bot_balance = bot.dbBot.balance || 0
        active_bots.set(Number(bot_balance), bot)
      }
    }
    if (!active_bots) throw new Error('No active bot')
    return active_bots.get(Array.from(active_bots.keys()).sort((a,b) => {return a - b;})[active_bots.size-1]); //выбор бота с наибольшим балансом
  }

  get_bots_promises() {
    //(примитивная реализация)
    var promises = []
    for (var bot of this.bots.values()) {
      if (!bot.error) promises.push(bot.proces)
    }
    return promises
  }

  count_active() {
    var active = 0
    for (var bot of this.bots.values()) {
      if (!bot.error) active++;
    }
    return active
  }

  async wait_any_bot_redy() {
    //дожидаемся завершения любого бота
    while (!this.stop) {
      var bot = null;
      var bots_promises = this.get_bots_promises()
      if (bots_promises.length != 0) {
        var bot = null
        var error = false

        try {
          bot = await Promise.race(bots_promises);
        } catch(e) {
          error = e // из за отчутввия try/catch/else в js
        }

        if (error) {
          var bot = error.bot
          delete error.bot
          console.log('eroor in bot',bot.id,error);
          if (error instanceof web_bot_errors.BaseError) {
            if (!error.on_final) {
              if (bot.retries < this.config.retries) {
                bot.retries++;
                bot.proces_transaction(bot.transaction);
              } else {
                bot.retries = null
                bot.error = true;
              }
            } else {
              if (bot.fin_retries < this.config.fin_retries) {
                bot.fin_retries++;
                bot.proces = bot;
              } else {
                bot.fin_retries = null
                bot.error = true;
              }
            }
          } else {
            bot.error = true;
          }
        } else {
          this.fin_retries = 1
          // возможна ситуация когда в Promise.race передаётся []
          // тогда выходим и получаем новый промис
          if (bot) {return}
          await promiseTimeout(500); // не блокируем node event loop
        }
      } else {
        // если все боты закончили и все с ошибкой - выходим
        if (this.count_active() == 0) return true

        if (this.stop) return false
        await promiseTimeout(500); // не блокируем node event loop
        // выход из цикла и взятие нового задания
        // только если есть бот который не заблокирован

      }
    }
  }

}
