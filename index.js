var bots_pool = require('./bots_pool');
var q         = require('./query');
var db_instance = require('./db/instance');
var fix = require('./fix_transactions');

var fs = require("fs");
var shema = require('./json_schemas');
var Validator = require('jsonschema').Validator;
var validator = new Validator();

var stop = false

var start_stoping = async (signal,inited_data) => {
    console.log('recived',signal,'stoping');
    stop = true;
    inited_data.bots_pull.stop = true;
    inited_data.query.stop = true;
}

exports.main_loop = async (inited_data) => {
  var {bots_pull, query, db, config} = inited_data;

  await fix.fixAll(db); //починка транзакций

  var transactions_to_reproc = await db.models.Transaction.findAll(
    { where: {state_id: db.models.transStateMap.redy }});
  for (var transaction_to_reproc of transactions_to_reproc) {
    var redy_bot = await bots_pull.get_redy_bot()
    if (stop) return
    await db.higlevel.updateBot(transaction_to_reproc, redy_bot.dbBot);
    await bots_pull.proces(transaction_to_reproc, redy_bot);
  }

  while(!stop) {
    //получение новой транзакции
    if(!bots_pull.count_active()) return // выходим, дальше process.exitCode = 1; т.к. stop == false
    var {message, msg} = await query.get_message();

    if (message) {
      //выбор бота и передача ему задания
      var redy_bot    = await bots_pull.get_redy_bot()
      if (!redy_bot) return // выходим, дальше process.exitCode = 1; т.к. stop == false
      let transaction = await db.higlevel.createTrans(message, redy_bot.dbBot);
      await query.ack(msg); // только при успешном создании забираем сообщение из очереди

      if (redy_bot.dbBot.balance && Number(redy_bot.dbBot.balance) < (message.sum * config.web_bot.course/100)) {
        await db.higlevel.setError(transaction, {message: "bots_no-money"})
        redy_bot.error = true;
      } else {
        if (transaction) await bots_pull.proces(transaction, redy_bot);
      }
    }
  }
}

exports.init = async () => {

  var config = JSON.parse(fs.readFileSync("config.json"));
  config.retries = config.retries || 1
  validator.validate(config, shema.serviceConfig, {throwError: true})

  var db = await db_instance.init_db(config.db)

  var query = new q.Query(config.query)
  await query.init()

  var bots_config = JSON.parse(fs.readFileSync("bot_config.json"));
  validator.validate(bots_config, shema.botConfig, {throwError: true})

  var bots_pull = new bots_pool.BotPool(bots_config, config.web_bot, db)
  await bots_pull.init();

  return { bots_pull, query, db, config };
}

exports.end = async (inited_data) => {
  await inited_data.db.sequelize.close();
  await inited_data.query.close()
}

if (require.main === module) {
  (async () => {
    var inited_data = await exports.init()
    var bots_pull = inited_data.bots_pull
    var query

    process.on('SIGTERM', () => {start_stoping('SIGTERM', inited_data)});
    process.on('SIGINT',  () => {start_stoping('SIGINT',  inited_data)});
    process.on('SIGPIPE', () => {start_stoping('SIGPIPE', inited_data)});

    try {
      var a = await exports.main_loop(inited_data);
    } catch (e) {
      console.log(e)
    }
    await exports.end(inited_data);
    if (stop) {
      process.exit(0);
    } else {
      process.exit(1);
    }

  })();
}
