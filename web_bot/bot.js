var wot = require('./wot');
var yandex = require('./yandex');
var instance = require('./instance');
var web_bot_errors = require('./errors');

exports.proces = async (bot, data) => {

  if (bot.nconfig.retries_pause && bot.retries > 1) {
    await new Promise(resolve => setTimeout(resolve, bot.nconfig.retries_pause));
  }

  bot.nightmare = instance.get_nigtmare(
    bot.nconfig.params,
    {
      proxy_uri     : bot.config.proxy,
      useragent     : bot.config.useragent,
      partition     : bot.id,
      userData_path : bot.nconfig.userData_path,
    }
  );

  try {
    bot.transaction = await bot.db.higlevel.setInProc(bot.transaction);
    await wot.proces(bot.nightmare, bot.config, data)

    await yandex.auth(bot.nightmare, bot.config, data)
    let yandex_data = await yandex.get_data(bot.nightmare, bot.config, data)

    try {
      await bot.db.higlevel.setYdata(bot.transaction,yandex_data);
    } catch (e) {
      if (e instanceof bot.db.Sequelize.UniqueConstraintError) {
        if (e.original.table == "Transactions" && e.original.constraint == "Transactions_y_hash_key") {
          throw new web_bot_errors.BaseError("yandex_prepay_hash_ununique")
        } else { throw e; }
      } else { throw e; }
    }

    await yandex.proces_toclick(bot.nightmare, bot.config, data)

    try {
      bot.transaction = await bot.db.higlevel.setYRedy(bot.transaction);
      await yandex.proces_click(bot.nightmare, bot.config, data)
      bot.transaction = await bot.db.higlevel.setYDone(bot.transaction);
      var yandex_post_data = await yandex.proces_postclick(bot.nightmare, bot.config, data)
    } catch (e) {
      e.on_final = true
      throw e
    }

    await bot.db.higlevel.setFinished(bot.transaction, yandex_post_data, yandex_data);

  } catch(e) {
    if (e instanceof web_bot_errors.BaseError) {

      var html, url, cookies, screenshot
      try {
        html = await bot.nightmare.evaluate(function () {return document.body.innerHTML});
      } catch (e) { console.log(e);}
      try {
        url = await bot.nightmare.evaluate(function () {return window.location.href});
      } catch (e) { console.log(e);}
      try {
        cookies = await bot.nightmare.cookies.get();
      } catch (e) { console.log(e);}
      try {
        var dimensions = await bot.nightmare.evaluate(function() {
            var body = document.querySelector('body');
            return {
                height: body.scrollHeight,
                width:body.scrollWidth
            }
        });
        await bot.nightmare.viewport(dimensions.width, dimensions.height)
        screenshot = await bot.nightmare.screenshot();
      } catch (e) { console.log(e);}
      data = {
        page: { html, url, cookies, selector: e.selector},
        screenshot: screenshot
      };

      bot.transaction = await bot.db.higlevel.setError(bot.transaction, e, undefined, data);
    } else {
      bot.transaction = await bot.db.higlevel.setError(bot.transaction, e, bot.db.models.transErrorMap.unknown)
    }
    throw e;
  } finally {
      await bot.nightmare.end();
  }
  return bot;
}
