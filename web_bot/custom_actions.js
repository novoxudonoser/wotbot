var bot_errors = require('./errors');
const assert = require('assert');
var Nightmare = require('nightmare')

Nightmare.action(
    'waitForUrl',
    function(ns, options, parent, win, renderer, done) {
        var navigationHistory = [];

        win.webContents.on('did-finish-load', function() {
            navigationHistory.push(win.webContents.getURL());
        });

        // Get the latest url
        parent.on('waitForUrl', function() {
            var url = navigationHistory[navigationHistory.length-1] || win.webContents.getURL()
            parent.emit('waitForUrl', url);
        });

        done();
    },
    function(matchUrl, done) {
        var self = this;

        var timeout = setTimeout(function() {
            return done(new Error('.waitForUrl(): timed out after ' + self.options.waitTimeout + 'ms - could not find a matching url'));
        }, self.options.waitTimeout);

        // Our event handler
        var handler = function(latestUrl) {
            if(latestUrl && latestUrl.match(matchUrl)) {
                self.child.removeListener('waitForUrl', handler);

                clearTimeout(timeout);
                done(null, true);
            } else {
                // If we don't match, emit again
                setTimeout(() => { self.child.emit('waitForUrl'); }, self.options.pollInterval);

            }
        };

        // Callback on waitForUrl
        self.child.on('waitForUrl', handler);
        self.child.emit('waitForUrl');
    }
);


var waitfn = async (f, timeout, step) => {
  var ok = false
  assert(step < timeout)
  for (var a=0; a < timeout/step; a++) {
    ok = await f();
    if(ok) break;
    await new Promise(resolve => setTimeout(resolve, step));
  }
  if (!ok) throw new bot_errors.TimeOutError(timeout);
}
exports.waitfn = waitfn


exports.chwait = async (n, selector, value, timeout) => {
  await n.wait((selector, value) => {
    return document.querySelector(selector).innerText != value
  }, selector, value);
  // await waitfn(async () => {
  //   if ( await n.evaluate((s, v) => {
  //     return document.querySelector(s).innerText != v
  //   },selector, value)) return true;
  // }, timeout || n.options.waitTimeout, n.options.pollInterval)
}

exports.wait = async (n, selectors, timeout) => {
  await n.wait((selectors) => {
    for (var i = 0; i < selectors.length; i++) {
      if (document.body && document.body.contains(document.querySelector(selectors[i]))) return true;
    }
  }, selectors);

  // await waitfn(async () => {
  //   for (var i = 0; i < selectors.length; i++) {
  //     return await n.evaluate((s) => {
  //       return document.body ? return document.body.contains(document.querySelector(s)) : false
  //     },selectors[i])
  //   }
  //   return false
  // }, timeout || n.options.waitTimeout, n.options.pollInterval)
}

exports.insert = async (n, selector, text, timeout) => {
  await waitfn(async () => {
    await n.insert(selector,text)
    if ( await n.evaluate(function (s, t) {
      var node = document.querySelector(s)
      return node ? node.value == t : false
    }, selector, text)) return true;
  }, timeout || n.options.waitTimeout, n.options.pollInterval)
}

exports.xwait = async (n, selectors, timeout) => {
  await exports.waitfn(async () => {
    for (var i = 0; i < selectors.length; i++) {
      var [s,r] = selectors[i];
      var present = await n.xpath(s, node => {
        return true;
      });
      if( !(present[0] == true ^ r) ) return true;
    }
  }, timeout || n.options.waitTimeout, n.options.pollInterval)
}

exports.goto = async (n, url) => {
  var result = await n.goto(url)
  if (result.code != 200) throw result
}
