var bot_errors = require('./errors');
var OTP = require('otp');
var Decimal = require('decimal.js');
var ca = require('./custom_actions')

exports.check_error = async (n, config, data) => {
  var s04 = 'h1[text()="Ошибка"]'
  var teherror
  try {
    teherror = await n.xpath(s04, node => {
      return true;
    });
  }  catch(e) {
    throw new bot_errors.BaseError('yandex_check-error_error_miss', [s04], e);
  }
  if (teherror == [true]) throw new bot_errors.BaseError('yandex_teh-error', [s04]);
}

exports.auth = async (n, config, data) => {
  await exports.check_error(n, config, data)

  //проверка авторизован ли
  var s02 = '//div[@class="lantern__title" and text()="Не получилось вас узнать"]'

  var not_authed
  try {
    not_authed = await n.xpath(s02, node => {
      return true;
    });
  }  catch(e) {
    throw new bot_errors.BaseError('yandex_auth_check_miss', [s02], e);
  }

  if (not_authed[0]) {
    var s1 = '.domik__content'
    try {
      await n.wait(s1);
    } catch(e) {
      throw new bot_errors.BaseError('yandex_auth_popup_timeout', [s1], e);
    }

    var s2 = '.auth__username .input__control'
    try {
      await n.insert(s2, config.y_login);
    } catch(e) {
      throw new bot_errors.BaseError('yandex_auth_miss_form-login', [s2], e);
    }

    var s3 = '.auth__password .input__control'
    try {
      await n.insert(s3, config.y_password);
    } catch(e) {
      throw new bot_errors.BaseError('yandex_auth_miss_form-y_password', [s3], e);
    }

    var s4 = '.auth__row_button_yes button'
    try {
      await n.click(s4);
    } catch(e) {
      throw new bot_errors.BaseError('yandex_auth_confirm_miss', [s4], e);
    }

    try {
      await ca.xwait(n, [[s02, false]])
    } catch(e) {
       throw new bot_errors.BaseError('yandex_auth_confirm_timeout', [s4], e);
    }


    var s5 = '.error-msg'
    try {
      var form_error = await n.exists(s5);
    } catch (e) {
      throw new bot_errors.BaseError('yandex_auth_form-error_miss', [s5], e);
    }
    if (form_error) {
      var s6 = '.error-msg'
      var error_text
      try {
        error_text = await n.evaluate(function (s6) {return document.querySelector(s6).innerText}, s6);
      } catch(e) {
        throw new bot_errors.BaseError('yandex_auth_form-error-content_miss', [s6], e);
      }
      throw new bot_errors.BaseError('yandex_auth_wrong', [s6], undefined,{error_text});
    }

    var not_second_authed
    try {
      not_second_authed = await n.xpath(s02, node => {
        return true;
      });
    }  catch(e) {
      throw new bot_errors.BaseError('yandex_auth_second_check_miss', [s02], e);
    }
    if (not_second_authed[0]) throw new bot_errors.BaseError('yandex_auth_second_check', [s02]);
  }
}

exports.get_data = async (n, config, data) => {

  //TODO тут может быть ошибка яндекса
  // выбор оплаты кошельком и извлечение данных о балансе
  var s8 = '.data-unit__base .button__text'
  var pay_method
  try {
    pay_method = await n.evaluate(function (s8) {return document.querySelector(s8).innerText}, [s8]);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_prepay_balance_pay-method_check', [s8], e);
  }


  var pay_methon_needed = "Мой кошелек"
  if (pay_method != pay_methon_needed) {

    var s9 = ".data-unit__base .button .button__text"
    try {
      await n.realMousedown(s9)
    } catch(e) {
      throw new bot_errors.BaseError('yandex_prepay_balance_pay-method-select_open', [s9], e);
    }

    var s01 = "//div[contains(@class, 'popup') and contains(@class, 'popup_visibility_visible')]//div[contains(@class,'select__item') and span/text()='Мой кошелек']"

    try {
      await ca.xwait(n, [[s01, true]])
    } catch(e) {
      throw new bot_errors.BaseError('yandex_prepay_balance_pay-method-open_timeout', [s01], e);
    }

    //клик на опции мой кошелёк
    var iner_paymethod_id
    try {
      iner_paymethod_id = await n.xpath(s01, node => {
        return node.id;
      });
    }  catch(e) {
      throw new bot_errors.BaseError('yandex_prepay_balance_pay-method-select_getid', [s01], e, {iner_paymethod_id});
    }
    var paymethod_selector = '#' + iner_paymethod_id
    try {
      await n.realClick(paymethod_selector);
    } catch(e) {
      throw new bot_errors.BaseError('yandex_prepay_balance_pay-method-select_choose-source', [paymethod_selector], e,
      {paymethod_selector});
    }

    var s02 = '//div[@class="data-unit__base"]//button/div[@class="button__text" and text()="Мой кошелек"]'

    try {
      await ca.xwait(n, [[s02, true]])
    } catch(e) {
      throw new bot_errors.BaseError('yandex_prepay_balance_pay-method-select_timeout', [s02], e);
    }

    var new_pay_method
    try {
      new_pay_method = await n.evaluate(function (s9) {return document.querySelector(s9).innerText}, s9);
    } catch(e) {
      throw new bot_errors.BaseError('yandex_prepay_balance_pay-method_final-check_select', [s9], e);
    }
    if (new_pay_method != pay_methon_needed) throw new bot_errors.BaseError('yandex_pay_balance_pay-method_final-check', [s9],
    {new_pay_method, iner_paymethod_id, paymethod_selector, pay_method});
  }

  //получение баланса бота
  var s11 = '.data-unit__base .button .button__right-hint .price'
  var bot_balance_raw
  try {
    bot_balance_raw = await n.evaluate(function (s11) {
      return document.querySelector(s11).innerText
    }, s11);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_prepay_balance_get', [s11], e);
  }

  //просмотр ошибки о недостатке средств
  var s12 = '.payment-contract__payment-types-lantern:not(.visibility_hidden_yes) .lantern__title'
  var has_money_error
  try {
    has_money_error = await n.evaluate(function (s12) {
      var selector = document.querySelector(s12)
      if (selector) return selector.innerText
    }, s12);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_prepay_balance_get', [s12], e);
  }

  if (has_money_error) {
    if (has_money_error = "В кошельке не хватает денег") {
      throw new bot_errors.BaseError('yandex_prepay_nomoney', [s12], {money_error: has_money_error});
    } else {
      throw new bot_errors.BaseError('yandex_prepay_nomoney_unknown', [s12], {money_error: has_money_error});
    }
  }

  var parse_sum = (sum_raw) => {
    return new Decimal(  (/^(\d+,\d\d|\d+) Р$/g).exec(sum_raw)[1].replace(/,/i,".") )
  }

  //получение атрибутов
  var s7 = {
    units: '.form__item > .data-unit',
    k: 'label',
    v: '.data-unit__base',
  }
  var page_data
  try {
    page_data = await n.evaluate((s7) => {
      var out_data = {};
      var units = document.querySelectorAll(s7.units);
      [].forEach.call(units, (unit) => {
        var k = unit.querySelector(s7.k).textContent;
        var v = unit.querySelector(s7.v).textContent;
        out_data[k] = v
      });
      return out_data
    }, s7);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_prepay_pay-data_extract', [s7], e);
  }

  try {
    var out_data = {
      bot_balance: parse_sum(bot_balance_raw),
      hash:        page_data['Номер заказа'],
      pname:       page_data['Идентификатор плательщика'],
      sum:         parse_sum(page_data['Сумма']),
      com:         parse_sum(page_data['В том числе комиссия']), // 0 Р
      sum_all:     parse_sum(page_data['Всего к оплате']), // =sum
    }
  } catch (e) {
    throw new bot_errors.BaseError('yandex_prepay_generate-outdata', [s12], e, {page_data, bot_balance_raw});
  }
  return out_data

}

exports.proces_toclick = async (n, config, data) => {

  var s13 = '.payment-contract__pay-button'
  try {
    await n.click(s13);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_pay_preconfirm_miss', [s13], e);
  }

  var s14 = '.secure-auth__sauth-action'
  try {
    await n.wait(s14);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_pay_preconfirm_timeout', [s14], e);
  }

  var otp = OTP(config.y_gauth);
  var code = otp.totp()
  var s15 = '.secure-auth__sauth-action .input__control'
  try {
    await n.insert(s15, code);
    await n.realClick(s15)
  } catch(e) {
    throw new bot_errors.BaseError('yandex_pay_otp_miss', [s15], e);
  }
  var s16 = '//div[contains(@class, "island__section")]/button[@disabled="disabled"]'
  try {
    await ca.xwait(n, [[s16, false]])
  } catch(e) {
     throw new bot_errors.BaseError('yandex_auth_confirm_otp_redy_timeout', [s16], e);
  }
}

exports.proces_click = async (n, config, data) => {
  var s15 = '.payment-contract__pay-button';
  try {
    await n.click(s15);
  } catch(e) {
    throw new bot_errors.BaseError('yandex_pay_pay-button_miss', [s15], e);
  }
}

exports.proces_postclick = async (n, config, data) => {
  var s03 = "//div[contains(@class, 'popup')]/div[@class='popup__content' and contains(text(), 'Похоже, пароль устарел.')]";
  var s05 = '//h1[text()="Платеж прошел"]';

  try {
    await ca.xwait(n, [[s03, true], [s05, true]])
  } catch(e) {
     throw new bot_errors.BaseError('yandex_postclick_status_timeout', [s03, s05], e);
  }


  var otp_error
  try {
    otp_error = await n.xpath(s03, node => {
      return true;
    });
  }  catch(e) {
    throw new bot_errors.BaseError('yandex_postclick_otp-error_miss', [s03], e);
  }
  if (otp_error[0]) throw new bot_errors.BaseError('yandex_postclick_otp-error', [s03]);

  var all_ok
  try {
    all_ok = await n.xpath(s05, node => {
      return true;
    });
  }  catch(e) {
    throw new bot_errors.BaseError('yandex_postclick_result_miss', [s05], e);
  }
  if (!all_ok[0]) throw new bot_errors.BaseError('yandex_postclick_result_error', [s05]);
}
