var Nightmare = require('nightmare');
require("nightmare-xpath");
const realMouse = require('nightmare-real-mouse');
realMouse(Nightmare);
require('./custom_actions')

exports.get_nigtmare = (params, bot_params) => {
  if (bot_params.proxy_uri) {
    params.switches = {
      'proxy-server': bot_params.proxy_uri,
      'ignore-certificate-errors': true
    }
  }
  params.webPreferences= {
    partition: "persist: "+bot_params.partition
  }
  params.paths = {
    userData: bot_params.userData_path
  }
  var n = new Nightmare(params);
  if (bot_params.useragent) n.useragent(bot_params.useragent);
  return n;
}
