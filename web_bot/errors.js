exports.BaseError = class BaseError extends Error {
  constructor(message, selector, secondary, sub) {
    super(message);
    this.selector = selector;
    this.secondary = secondary
    this.sub = sub

    this.message  = message;
    this.name = this.constructor.name;
  }
}

exports.TimeOutError = class TimeOutError extends Error {
  constructor(timeout) {
    var message = "timed out after " + timeout + " ms"
    super(message);
    this.timeout = timeout;

    this.message  = message;
    this.name = this.constructor.name;
  }
}
