var bot_errors = require('./errors');
var custom_actions = require('./custom_actions')
var Decimal = require('decimal.js');

exports.proces = async (n, config, data) => {
  await exports.start(n, config, data);

  var was_auth = false;
  var is_authed = await exports.check_auth(n, config, data);
  if (!is_authed) {
    await exports.auth(n, config, data);
    await exports.start(n, config, data);
    await exports.wait_reauth(n, config, data)
  }
  await exports.to_yandex(n, config, data);
}



exports.auth = async (n, config, data) => {

  var url1 = 'http://worldoftanks.ru/auth/oid/new/';
  try {
    await custom_actions.goto(n, url1);
  } catch(e) {
    throw new bot_errors.BaseError('wot_auth_network', undefined, e, {url: url1});
  }

  // var url2 = /https:\/\/ru\.wargaming\.net\/id\/signin*/
  // try {
  //   await n.waitForUrl(url2);
  // } catch (e) {
  //   throw new bot_errors.BaseError('wot_auth_network', undefined, e, {url: url2.toString()});
  // }
  n.wait(5000) //грязный временный фикс


  var s9 = '.cm-user-menu-link_cutted-text'
  var s2 = '#id_login'
  try {
    await custom_actions.wait(n, [s2, s9])
  } catch(e) {
    throw new bot_errors.BaseError('wot_auth_form_timeout', [s2, s9], e);
  }

  if (!await exports.check_auth(n, config, data)) {
    var w_login = config.w_login
    try {
      await n.insert(s2, w_login);
    } catch(e) {
      throw new bot_errors.BaseError('wot_auth_form_login_miss', [s2], e);
    }
    var s3 = '#id_password'
    var w_password = config.w_password
    try {
      await n.insert(s3, w_password);
    } catch(e) {
      throw new bot_errors.BaseError('wot_auth_form_password_miss', [s3], e);
    }

    var s4 = '#id_remember'
    try {
      await n.check(s4);
    } catch(e) {
      throw new bot_errors.BaseError('wot_auth_form_remember_miss', [s4], e);
    }

    var s5 = '.b-form_fieldgroup  button[type="submit"]'
    try {
      await n.click(s5);
    } catch(e) {
      throw new bot_errors.BaseError('wot_auth_confirm_miss', [s5], e);
    }

    var url3 = /http:\/\/worldoftanks\.ru\/$/
    try {
      await n.waitForUrl(url3);
    } catch (e) {
      throw new bot_errors.BaseError('wot_auth_confirm_network', undefined, e, {url: url3});
    }

    var s6 = '.b-form .js-form-errors-content'
    var s8 = '.b-form .jsc-captcha-input[style*="display: inline;"]'
    var s9 = '.cm-user-menu-link_cutted-text'

    try {
      await custom_actions.wait(n, [s6, s8, s9])
    } catch(e) {
      throw new bot_errors.BaseError('wot_auth_confirm_timeout', [s6, s8, s9], e);
    }

    try {
      var form_error = await n.exists(s6);
    } catch (e) {
      throw new bot_errors.BaseError('wot_auth_form-error_miss', [s6], e);
    }
    if (form_error) {

      var s7 = '.b-form .js-form-errors-content'
      try {
        var error_text = await n.evaluate(function (s7) {return document.querySelector(s7).innerText}, s7);
      } catch(e) {
        throw new bot_errors.BaseError('wot_auth_form-error-content_miss', [s7], e);
      }

      var r1 = (/^Неверный email или пароль\.$/g)
      if (r1.exec(error_text)) {
        throw new bot_errors.BaseError('wot_auth_wrong', [s7], {error_text});
      }
      else {
        throw new bot_errors.BaseError('wot_auth_wrong_unknown', [s7], {error_text});
      }
    }

    try {
      var captcha = await n.exists(s8);
    } catch (e) {
      throw new bot_errors.BaseError('wot_auth_miss_captcha', [s8], e);
    }
    if (captcha) throw new bot_errors.BaseError('wot_auth_captcha', [s8]); //captcha
  }
}

exports.check_auth = async (n, config, data) => {
  var s10 = '.cm-user-menu-link_cutted-text'
  try {
    return await n.exists(s10);
  } catch(e) {
    throw new bot_errors.BaseError('wot_auth_check_failed', [s10], e);
  }
}

exports.wait_reauth = async (n, config, data) => {
  // Ожидаем повторную перезагрузку страницы с авторизацией
  // waitForUrl выполниться только когда страница либо уже загружаеться либо начнёт загружатся
  // и останоиться. В данном случае выполняется 2й вариант.
  var url2 = 'https://ru.wargaming.net/shop/wot/gold/34/'
  try {
    await n.waitForUrl(url2);
  } catch (e) {
    throw new bot_errors.BaseError('wot_buy_network_wait-url', undefined, e, {url: url2});
  }

  var s10 = '.cm-user-menu-link_cutted-text'
  try {
    await custom_actions.wait(n, [s10])
  } catch (e) {
    throw new bot_errors.BaseError('wot_auth_second_check_failed', [s10], e);
  }
}

exports.start = async (n, config, data) => {
  var url2 = 'https://ru.wargaming.net/shop/wot/gold/34/'

  try {
    await custom_actions.goto(n, url2);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_network', undefined, e, {url: url2});
  }

  try {
    await n.waitForUrl(url2);
  } catch (e) {
    throw new bot_errors.BaseError('wot_buy_network_wait-url', undefined, e, {url: url2});
  }
}

exports.to_yandex = async (n, config, data) => {
  var s11 = '.item-header_buttons a'
  var s12 = '.gift'

  try {
    await custom_actions.wait(n, [s11, s12])
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_buy-button_timeout', [s5], e);
  }

  var new_d
  try {
     new_d = await n.exists(s11);
  } catch(e) {
    console.log(e);
  }

  var old_d
  try {
     old_d = await n.exists(s12);
  } catch(e) {
    console.log(e);
  }

  if (new_d & old_d) throw new Error();
  if (!new_d & !old_d) throw new bot_errors.BaseError('wot_buy_buy-button_miss', [s11,s12]);
  if (new_d) {await exports.to_yandex_new_design(n, config, data);}
  if (old_d) {await exports.to_yandex_old_design(n, config, data);}

  //детектирование перехода или ошибки
  var s13 = '.form-row_message'

  try {
    await n.waitForUrl(/https:\/\/money\.yandex\.ru\/.*/);
  }  catch(e) {

    try {
      await custom_actions.wait(n, [s13])
    } catch(e) {
      throw new bot_errors.BaseError('wot_redirect-error_form-error_miss', [s13], e);
    }

    var s14 = '.form-row_message'
    try {
      var error_text = await n.evaluate(function (s14) {return document.querySelector(s14).innerText}, s14);
    } catch(e) {
      throw new bot_errors.BaseError('wot_redirect-error_form-error_miss_text', [s14], e);
    }

    var error
    var r1 = (/^Убедитесь, что это значение содержит не более 24 символов \(сейчас \d+\)\.$/g)
    if (r1.exec(error_text)) {
      throw new bot_errors.BaseError('wot_popup_nouser_tobig', [s14], undefined, {error_text});
    }
    var r2 = (/^Пользователь с таким именем не найден\.$/g)
    if (r2.exec(error_text)) {
      throw new bot_errors.BaseError('wot_popup_nouser', [s14], undefined, {error_text});
    }
    if (!error) {
      throw new bot_errors.BaseError('wot_popup_nouser_unknown', [s14], undefined, {error_text});
    }
  }
}

exports.insert_sum = async (n, config, data) => {
  var sum = data.sum
  var s15 = '.form-inputtext__amount'

  var page_sum_raw
  try {
    page_sum_raw = await n.evaluate((s) => {return document.querySelector(s).value}, s15);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_sum_get', [s15], e);
  }

  var s17 = '.price > span:nth-child(1)'
  var page_money_raw_start
  try {
    page_money_raw_start = await n.evaluate((s) => {return document.querySelector(s).innerText}, s17);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_sum_get_miss', [s17], e);
  }

  if (Number(page_sum_raw) != sum ) {
    try {
      await n.evaluate((s) => {document.querySelector(s).value= ""}, s15);
    } catch(e) {
      throw new bot_errors.BaseError('wot_buy_sum_put_clear', [s15], e);
    }

    var s16 = '.form-inputtext__amount'
    var sum_str = sum.toString()
    try {
      await n.insert(s16, sum_str);
    } catch(e) {
      throw new bot_errors.BaseError('wot_buy_sum_put_insert', [s16], e);
    }

    try {
      await custom_actions.chwait(n, s17, page_money_raw_start)
    } catch(e) {
      throw new bot_errors.BaseError('wot_buy_sum_change_timeout', [s17], e);
    }
  }

  var page_money_raw
  try {
    page_money_raw = await n.evaluate((s) => {return document.querySelector(s).innerText}, s17);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_sum_check_miss', [s17], e);
  }


  //проверка правильного курса и суммы
  var final_sum
  var rg = (/^(\d+,\d\d) ₽$/g).exec(page_money_raw)
  var ok = false
  if (rg) {
    if (rg.length == 2) {
      var summ_mas_raw = rg[1].split(",")
      final_sum = Number(summ_mas_raw[0]) * 100 + Number(summ_mas_raw[1])
      ok = true
    }
  }
  if (!ok) {
    throw new bot_errors.BaseError('wot_buy_sum-money_check_extract', [s17], undefined, {page_money_raw});
  }

  var course = data.course
  if (Math.abs(Math.round(final_sum / 100)  - Math.round(sum  * course / 100)) > 1) {
    throw new bot_errors.BaseError('wot_buy_sum-money_rate', [s15 ,s17], undefined, {
      sum,
      page_money_raw,
      final_sum,
      rate: course
    });
  }

}

exports.insert_to = async (n, config, data) => {
  var to = data.to;
  var comment = data.comment;
  var s18 = '#id_gift_recipient'
  try {
    await custom_actions.wait(n, [s18])
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_pay-to-insert_timeout', [s18], e);
  }
  try {
    await custom_actions.insert(n, s18, to);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_pay-to-insert_miss', [s18], e);
  }
  var s19 = '#id_gift_message'
  try {
    await custom_actions.insert(n, s19, comment);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_pay-to-insert-message_miss', [s19], e);
  }
}


exports.to_yandex_new_design = async (n, config, data) => {
  // заполнения суммы
  await exports.insert_sum(n, config, data)

  //открытие попапа
  var s20 = '.item-header_buttons a'
  try {
    await n.click(s20);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_open_miss', [s20], e);
  }

  var s21 = '.purchase-popup'

  try {
    await custom_actions.wait(n, [s21])
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_open_timeout', [s21], e);
  }

  //открытие покупки в подарок
  var s22 = '.tab a:nth-child(2)'
  try {
    await n.click(s22);
  } catch(e) {
    throw new bot_errors.BaseError('wot_popup_select-donum_miss', [s22], e);
  }


  var s23 = '.animated-block'
  try {
    await custom_actions.wait(n, [s23])
  } catch(e) {
    throw new bot_errors.BaseError('wot_popup_select-donum_open-timeout', [s21], e);
  }

  var ab
  try {
    ab = n.visible(s23)
  } catch(e) {
    throw new bot_errors.BaseError('wot_popup_select-donum_miss', [s23], e);
  }
  if (!ab) throw new bot_errors.BaseError('wot_popup_select-donum_timeout', [s23]); //не открылся попап

  //ввод получателя
  await exports.insert_to(n, config, data)

  //переход на яндекс
  var s24 = '#button-4'
  try {
    await n.click(s24);
  } catch(e) {
    throw new bot_errors.BaseError('wot_popup_goto-yandex_miss', [s24], e);
  }
}



exports.to_yandex_old_design = async (n, config, data) => {

  await exports.insert_sum(n, config, data)

  var s25 = '.gift'
  try {
    await n.click(s25);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_open_miss', [s25], e);
  }

  var s26 = '.popup'
  try {
    await custom_actions.wait(n, [s26])
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_open_timeout', [s26], e);
  }

  //ввод получателя
  await exports.insert_to(n, config, data)

  //переход на яндекс
  var s27 = '.popup .payment-button_purchase'
  try {
    await n.click(s27);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_select-method_miss', [s27], e);
  }

  var s28 = '.popup .payment-method_dropdown'
  try {
    await n.wait(s28);
  } catch(e) {
    throw new bot_errors.BaseError('wot_buy_popup_select-method_timeout', [s28], e);
  }

  var s29 = '.popup .ga-payment-group-4'
  try {
    await n.click(s29);
  } catch(e) {
    throw new bot_errors.BaseError('wot_popup_goto-yandex_miss', [s29], e);
  }

}
