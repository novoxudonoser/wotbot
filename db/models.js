
module.exports = (sequelize, Sequelize) => {
  var exp = {}

  exp.transStateMap = {
    income       : 1,
    redy         : 2,
    in_progres   : 3,
    fineshed_ok  : 5,
    fin_unknown  : 6,
    error        : 7,
    waiting      : 8,
  };

  exp.transProgresMap = {
    none         : 1,
    wot_started  : 2,
    y_click_redy : 3,
    y_click_done : 4,
  };

  exp.transErrorMap = {
    unknown      : 'unknown',
  };

  exp.Transaction = sequelize.define('Transactions', {
    id          : {
      type: Sequelize.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    id_uuid     : {
      type: Sequelize.UUID(),
      allowNull: false,
      defaultValue: Sequelize.UUIDV4,
      unique: true,
    },
    sum         : {type: Sequelize.BIGINT(), allowNull: false},
    wot_user_to : {type: Sequelize.STRING(30), allowNull: false},
    comment     : {type: Sequelize.TEXT},
    y_hash      : {
      type: Sequelize.STRING(32),
      unique: true,
    },
    created_at  : {type: Sequelize.DATE, allowNull: false},

    state_at    : {type: Sequelize.DATE, allowNull: false},
    state_id    : {type: Sequelize.INTEGER, allowNull: false},
    progres_id  : {type: Sequelize.INTEGER, allowNull: false},

    error_code  : {type: Sequelize.STRING(60)},

    bot_id : {
      type: Sequelize.BIGINT,
      allowNull: false,
      references: {
        model: "Bots",
        key: "id"
      }
    },
  },{
    timestamps: false,
    freezeTableName: true,
  });

  exp.TransactionState = sequelize.define('TransactionsHistory', {
    id             : {
      type: Sequelize.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    transaction_id : {
      type: Sequelize.BIGINT,
      allowNull: false,
      references: {
        model: "Transactions",
        key: "id"
      }
    },
    bot_id : {
      type: Sequelize.BIGINT,
      allowNull: false,
      references: {
        model: "Bots",
        key: "id"
      }
    },
    state_from    : {type: Sequelize.INTEGER, allowNull: false},
    state_to      : {type: Sequelize.INTEGER, allowNull: false},
    progres_from  : {type: Sequelize.INTEGER, allowNull: false},
    progres_to    : {type: Sequelize.INTEGER, allowNull: false},
    state_at      : {type: Sequelize.DATE, allowNull: false},
    error_code    : {type: Sequelize.STRING(60)},
  },{
    timestamps: false,
    freezeTableName: true,
  });


  exp.Transaction.hasMany(exp.TransactionState,
    { as: 'StetesLog',foreignKey: 'transaction_id'})


  exp.Bot = sequelize.define('Bots', {
    id      : {
      type: Sequelize.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    id_uuid : {
      type: Sequelize.UUID(),
      allowNull: false,
      defaultValue: Sequelize.UUIDV4,
      unique: true,
    },
    balance  : {type: Sequelize.DECIMAL(10,2)},
  },{
    timestamps: false,
    freezeTableName: true,
  });

  exp.ErrorData = sequelize.define('ErrorsData', {
    id      : {
      type: Sequelize.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    screenshot : {type: Sequelize.BLOB},
    data       : {type: Sequelize.JSONB},
    error_code : {type: Sequelize.STRING(60)},
    transaction_id : {
      type: Sequelize.BIGINT,
      allowNull: false,
      references: {
        model: "Transactions",
        key: "id"
      }
    },
    transaction_hist_id : {
      type: Sequelize.BIGINT,
      allowNull: false,
      references: {
        model: "TransactionsHistory",
        key: "id"
      }
    },
  },{
    timestamps: false,
    freezeTableName: true,
  });

  exp.ErrorData.belongsTo(exp.Transaction, {  as: 'Transaction',foreignKey: 'transaction_id'})
  exp.ErrorData.belongsTo(exp.TransactionState, {  as: 'TransactionState',foreignKey: 'transaction_hist_id'})

  exp.Bot.hasMany(exp.Transaction,{ as: 'Bot',foreignKey: 'bot_id'})
  exp.Transaction.belongsTo(exp.Bot, {  as: 'Bot',foreignKey: 'bot_id'})
  exp.Bot.hasMany(exp.TransactionState,{ as: 'Bot',foreignKey: 'bot_id'})
  exp.TransactionState.belongsTo(exp.Bot, {  as: 'Bot',foreignKey: 'bot_id'})

  return exp
}
