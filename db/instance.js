var Sequelize = require("sequelize");
var cls = require('continuation-local-storage');
var namespace = cls.createNamespace('sails-sequelize-#{connection.database}');

Sequelize.cls = namespace;

exports.init_db = async (config) => {
  var sequelize = new Sequelize(config.uri, config.params)

  var models   = require('./models')(sequelize, Sequelize)
  var higlevel = require('./higlevel')(sequelize, Sequelize, models)

  var db = {
    models: models,
    higlevel: higlevel,
    sequelize: sequelize,
    Sequelize: Sequelize,
  }

  await sequelize.authenticate().then(function() {
      console.log('Connect to DB created!');
  }).catch(function(err) {
      console.log('Connection error: ' + err);
  });

  return db
}
