
var Decimal = require('decimal.js');
var assert = require('assert')
const serializeError = require('serialize-error');

var bot_errors = require('./../web_bot/errors');

module.exports = (sequelize, Sequelize, models) => {
  var exp = {}

  exp.getBot = async (params) => {
    var time_now = new Date();
    return await sequelize.transaction((t) => {
      return models.Bot.findOne({ where: {id_uuid: params.uuid} }, {transaction: t}).then(bot => {
        if (!bot) {
          return models.Bot.build({
            id_uuid: params.uuid,
          }).save({transaction: t});
        } else {
          return bot
        }
      })
    })
  }

  exp.createTrans = async (params, dbBot) => {
    var time_now = new Date();

    var trans_params = {
      id_uuid     : params.uuid        ,
      sum         : params.sum         ,
      wot_user_to : params.user_to     ,
      comment     : params.comment     ,
      y_hash      : null               ,
      created_at  : time_now           ,
      state_at    : time_now           ,
      state_id    : models.transStateMap.redy    ,
      progres_id  : models.transProgresMap.none  ,
      error_code  : null               ,
      bot_id      : dbBot.id           ,
      StetesLog : [{
        state_from   : models.transStateMap.income ,
        state_to     : models.transStateMap.redy   ,
        progres_from : models.transProgresMap.none ,
        progres_to   : models.transProgresMap.none ,
        state_at     : time_now,
        bot_id       : dbBot.id           ,
      }]
    };

    try {
      return await sequelize.transaction((t) => {
        return models.Transaction
          .build(trans_params,
            {
              include: [{
                model: models.TransactionState,
                as: 'StetesLog'
              }]
            })
          .save();
      });
    } catch (e) {
      if (e instanceof Sequelize.UniqueConstraintError) {
        if (e.original.table == "Transactions" && e.original.constraint == "Transactions_id_uuid_key") {
          console.log("ERROR UNUNIQUE TRANSACTION UUID",params);
        } else { throw e; }
      } else { throw e; }
    }
  };

  exp.move_to_next_status = async (trans, next_state, next_progres, error_code, other_atrs) => {
    var time_now        = new Date();
    var current_state   = trans.state_id;
    var current_progres = trans.progres_id;

    var new_trans_attrs = {
      state_id   : next_state   ,
      progres_id : next_progres ,
      state_at   : time_now     ,
      error_code : error_code || null  ,
    };

    return models.Transaction.update(Object.assign(new_trans_attrs,other_atrs),
      {where: {id: trans.id, state_id: current_state, progres_id: current_progres}})
      .then(res => {
        if (!res[0]) throw new Error("TRANSACTION "+trans.uuid+" NOT CONSISTENT");
        var new_state_change = {
          state_from     : current_state       ,
          state_to       : next_state          ,
          progres_from   : current_progres     ,
          progres_to     : next_progres        ,
          state_at       : time_now            ,
          error_code     : error_code          ,
          transaction_id : trans.id            ,
          bot_id         : trans.bot_id        ,
        };
        return models.TransactionState.build(new_state_change).save()
      });
  };

  exp.setError = async (trans, error, error_code, data) => {
    error_code = error_code ? error_code : error.message;
    data = data ? data : {page: {}};
    var to_state = (error.on_final) ? models.transStateMap.fin_unknown : models.transStateMap.error
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {
      return exp.move_to_next_status(
        trans,
        to_state,
        models.transProgresMap.none,
        error_code
      ).then(trans_state => {
        var error_data = {
          screenshot: data.screenshot,
          error_code: error_code,
          data: Object.assign(data.page, {
            stack: error.stack,
            secondary: serializeError(error.secondary),
          }, error.sub),
          transaction_id: trans_state.transaction_id,
          transaction_hist_id: trans_state.id,
        };
        return models.ErrorData.build(error_data).save({transaction: t}).then(error_data => {
          var a = models.Transaction.findById(error_data.transaction_id, {transaction: t})
          return a
        });;
      });
    });
  };

  exp.setInProc = async (trans) => {
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {
      return exp.move_to_next_status(
        trans,
        models.transStateMap.in_progres,
        models.transProgresMap.wot_started
      ).then(trans_state => {
        var a = models.Transaction.findById(trans_state.transaction_id, {transaction: t})
        return a
      });
    });
  };


  exp.setYdata = async (trans, data) => {
    try {
      assert(data.com.equals( Decimal(0) ))
      assert(data.sum.equals( data.sum_all ))
    } catch (e) {
      if ( e instanceof assert.AssertionError ) {
        throw new bot_errors.BaseError('yandex_save-data_validate');
      } else throw e
    }
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {

      return models.Transaction.update({y_hash: data.hash},
      {where: {id: trans.id}}).then(ntrans => {
        return models.Bot.update({balance: data.bot_balance.toString()},
        {where: {id: trans.bot_id}});
      });
    });
  };


  exp.setYRedy = async (trans, data) => {
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {
      return exp.move_to_next_status(
        trans,
        models.transStateMap.in_progres,
        models.transProgresMap.y_click_redy
      ).then(trans_state => {
        return models.Transaction.findById(trans_state.transaction_id, {transaction: t})
      });
    });
  };

  exp.setYDone = async (trans) => {
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {
      return exp.move_to_next_status(
        trans,
        models.transStateMap.in_progres,
        models.transProgresMap.y_click_done
      ).then(trans_state => {
        return models.Transaction.findById(trans_state.transaction_id, {transaction: t})
      });
    });
  };

  exp.setFinished = async (trans, yandex_post_data, yandex_data) => {
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {
      return exp.move_to_next_status(
        trans,
        models.transStateMap.fineshed_ok,
        models.transProgresMap.none
      ).then(trans_state => {
        return models.Bot.findById(trans_state.bot_id, {transaction: t}).then( bot => {
          return bot.decrement(['balance'], { by: yandex_data.sum.toString() }, {transaction: t})
        });
      });
    });
  };

  exp.updateBot = async (trans, bot) => {
    return await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.REPEATABLE_READ
    },(t) => {
      return trans.setBot(bot, {transaction: t})
    });
  };

  return exp
}
