var db_instance = require('./instance');

var fs = require("fs");
var shema = require('./../json_schemas');
var Validator = require('jsonschema').Validator;
var validator = new Validator();

if (require.main === module) {
  (async () => {
    var config = JSON.parse(fs.readFileSync("config.json"));
    validator.validate(config, shema.serviceConfig, {throwError: true})
    var db = await db_instance.init_db(config.db)

    try {
      await db.sequelize.sync({force: true})
    } catch (e) {
      throw e;
    }
  })()
}
